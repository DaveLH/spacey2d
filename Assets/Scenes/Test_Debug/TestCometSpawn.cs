﻿using UnityEngine;
using System.Collections;

public class TestCometSpawn : MonoBehaviour
{
    public GameObject cometPrefab;

    public Transform spawnPoint;

	
	void Start ()
    {
        spawnPoint = this.transform;

        InvokeRepeating("SpawnComets", 2.0f, 4.0f);
	}
	
	
	void SpawnComets()
    {
        GameObject.Instantiate(cometPrefab, spawnPoint.position, spawnPoint.rotation);
	}
}
