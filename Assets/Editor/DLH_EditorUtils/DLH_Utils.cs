﻿using UnityEngine;
using UnityEditor;
using System.Collections;


namespace DLH_UnityLib
{
    public class DLH_Utils : MonoBehaviour
    {
        // Quickly copy an object's tag to its children 
        //
        [MenuItem("DLH Utls/Copy Tag to Children")]
        //
        static void CopyTagToChildren()
        {
            Transform trRoot = Selection.activeTransform;

            string sTag = trRoot.tag;

            foreach (Transform child in trRoot)
            {
                child.tag = sTag;
            }
        }
    }

}
