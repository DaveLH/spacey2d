﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Base class for Black holes and wormholes -- Control motion and rotation
/// </summary>
/// 
public class SwirlyThing : MonoBehaviour
{
    public float moveRate;     // How far in units object moves forward in a second
    public float rotateRate;   // Degrees object rotates in a second

    protected float fXSpeed, m_x, m_z, m_angle;   // Handle motion and rotation animation

    // If object is wormhole, proceed to next level, else player "dies"
    //
    public virtual bool IsWormhole
    {
        get { return false; }
    }

    protected virtual void Start()
    {
        // Set random angle of motion (as with asteroids)
        //
        fXSpeed = Random.Range(-1.0f * moveRate, 1.0f * moveRate);  // Magnitude of sideways motion
    }

    protected virtual void Update()
    {
        m_x = (fXSpeed  * Time.deltaTime);
        m_z = (moveRate * Time.deltaTime);

        m_angle = rotateRate * Time.deltaTime;

        // Move object "forward"
        //
        transform.Translate(m_x, 0f, m_z, Space.World);

        // Rotate vortex
        //
        transform.Rotate(Vector3.forward, m_angle); 
    }


    protected virtual void OnTriggerEnter(Collider other)
    {

    }
}
