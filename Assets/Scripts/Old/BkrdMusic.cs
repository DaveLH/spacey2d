﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Handle seamless play of background music
/// </summary>
/// 
public class BkrdMusic : MonoBehaviour
{
	void Awake ()
    {
        GameObject[] gobs = GameObject.FindGameObjectsWithTag("BkrdAudio");

        if (gobs.Length > 1) Destroy(this.gameObject);   // Ensure only one is ever created
        //
        else DontDestroyOnLoad(this.gameObject);   // Make music continually play
	}
}
