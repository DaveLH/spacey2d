﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Interface to simulate the accretion of planets,
///   Either the "Earth's Twin" version or (TODO) the full-fledged
///    ACCRETE simulation (Dole, Issacman, Sagan), or any contemporary counterpart
/// </summary>
/// 
public interface IAccrete
{
    // Simulate a protoplanet's sweeping up of dust and gas
    //
    void AccreteSim(PlanetData pd);

    // Check for planets that will likely collide, and merge them
    //
    void PlayPoolWithPlanets();
}
