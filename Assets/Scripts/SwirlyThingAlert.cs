﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Make objects react to hitting black/wormhole
/// </summary>
/// <remarks>
/// OWNED BY: Player and Enemy objects
/// </remarks>
/// 
public class SwirlyThingAlert : MonoBehaviour
{
    public float MaxVelocity = 5f;   // Maximum speed as it falls into the well
    public float ObjTurnTime = 0.3f;  // Time it takes a captured object to turn towards hole

    protected Rigidbody m_rigidBody;   // Object's rigid body

    protected Transform m_trHorizon = null;   // Black or wormhole that has captured player

    protected bool m_bCapturedByThing = false;   // Captured by black/wormhole?
    protected bool m_bReachedHorizon  = false;   // Have we reached the event horizon?
    protected bool m_bWereGoingToDie  = false;   // Is this black hole the fatal kind, or a travesible wormhole?

    protected float m_fVelocty = 0f;  // Current velocity of free-fall

    protected Vector3 m_v3DestPos;   // Destination position for ship to slip smoothly into hole
    protected Vector3 m_v3TargScl;   // Target scale ("Spaghettification")


    public Rigidbody rigidBody
    {
        get { return m_rigidBody; }
        set { m_rigidBody = value; }
    }

    public Transform eventHorizon
    {
        get { return m_trHorizon; }
        set { m_trHorizon = value; }
    }

    /// <summary>
    /// Get/Set "Swirly Thing" behavior
    /// </summary>
    /// 
    public bool Activated
    {
        get { return m_bCapturedByThing; }
        set { m_bCapturedByThing = value; }
    }


    void Start ()
    {
        m_rigidBody = GetComponent<Rigidbody>();
	}


    /// <summary>
    /// Handle object getting captured by hole
    /// </summary>
    /// <param name="other"></param>
    /// 
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "SwirlyThing")  // Caught by hole's influence
        {
            // Black hole type determines if game ends or proceeds to next level
            //
            m_bWereGoingToDie = !(other.transform.GetComponent<SwirlyThing>().IsWormhole);

            // Start running "fall into vortex" animation
            //
            m_trHorizon = other.transform.Find("EventHorizon");
            //
            rigidBody.isKinematic = true;   // Take full contol of motion
            m_bCapturedByThing    = true;

            // Prepare to align ship with hole
            //
            m_v3DestPos = new Vector3(0.0f, 0.0f, transform.position.z);
            m_v3TargScl = new Vector3(0.1f, 1.0f, 2.0f);

            if (!m_bWereGoingToDie)
            {
                GameController.StopEnemySpawn(); // Stop spawning enemies
                GameController.StartFadeOut();   // Start fading out to next scene
            }
        }
        else if (other.tag.Contains("EventHorizon"))  // Event Horizon
        {
            m_bReachedHorizon = true;
        }
    }


    void FixedUpdate ()
    {
        if (m_bCapturedByThing)
        {
            if (!m_bReachedHorizon)
            {
                FUpdate_TurnObjTowards();
                FUpdate_SuckInObject(); 
            }
            else  // Object has reached event horizon and will disappear
            {
                DeathByBlackHole();
            }
        }
    }


    /// <summary>
    /// Handle object's accelerated fall into vortex
    /// </summary>
    /// <remarks>
    /// CALLED BY: FixedUpdate()
    /// </remarks>
    /// 
    void FUpdate_SuckInObject()
    {
        float fHorizPos = transform.position.x;

        // Center ship so it goes through wormhole
        //
        if (Mathf.Abs(fHorizPos) > 0.01f)
        {
            transform.position = Vector3.Lerp(transform.position, m_v3DestPos, Time.deltaTime); 
        }

        // Simulate gravitational "Spaghettification" from black hole
        //
        transform.localScale = Vector3.Lerp(transform.localScale, m_v3TargScl, Time.deltaTime / 2.0f);
    }


    /// <summary>
    /// Handle animation of Player getting sucked into vortex
    /// </summary>
    /// <remarks>
    /// CALLED BY: FixedUpdate()
    /// </remarks>
    /// 
    void FUpdate_TurnObjTowards()
    {
        if (m_trHorizon)
        {
            // Get angle representing direction to hole
            //
            Quaternion qRot = Quaternion.LookRotation(m_trHorizon.transform.position - transform.position);

            // Turn towards hole
            //
            rigidBody.MoveRotation(Quaternion.RotateTowards(transform.rotation, qRot, ObjTurnTime)); 
        }
    }

    /// <summary>
    /// Handle what happens when object reaches center of hole
    /// </summary>
    /// 
    protected virtual void DeathByBlackHole()
    {
        if (!m_bWereGoingToDie)
        {
            // End level -- Start next Level
            //
            GameController.StartNextLevel(); 
        }

        // Else object will be destroyed by "DestroyByContact" and game will end
    }
}
