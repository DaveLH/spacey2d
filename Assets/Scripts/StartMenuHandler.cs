﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Handle Events in the Start Menu
/// </summary>
/// 
public class StartMenuHandler : MonoBehaviour
{
    GameController m_gameCtrlr;   // Reference to Game Controller 


	void Start ()
    {
        GameObject gobGameCtrlr = GameObject.Find("Game Controller");

        m_gameCtrlr = gobGameCtrlr.GetComponent<GameController>();
	}
	
    public void OnPlayBtn()
    {
        m_gameCtrlr.StartPlaying();
    }
}
