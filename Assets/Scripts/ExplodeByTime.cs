﻿using UnityEngine;
using System.Collections;

public class ExplodeByTime : MonoBehaviour
{
	public float lifetime;

    public GameObject explosion;

    void Start ()
	{
        StartCoroutine(TimedExplode());
	}


    IEnumerator TimedExplode()
    {
        yield return new WaitForSeconds(lifetime);

        Instantiate(explosion, transform.position, transform.rotation);

        Destroy(gameObject);
    }
}
