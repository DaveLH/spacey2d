﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Handle mutual anihilation when two objects collide
/// </summary>
/// <remarks>
/// OWNER: All objects that can do damage to player's ship
/// </remarks>
/// 
public class Done_DestroyByContact : MonoBehaviour
{
	public GameObject explosion;
	public GameObject playerExplosion;

    public int scoreValue;

    GameController gameController;


	void Start ()
	{   
	}


	void OnTriggerEnter (Collider other)
	{
		if (other.tag == "Boundary" || other.tag == "Enemy" || other.tag == "TriggerRegion" ||
            other.tag == "Planet" || other.tag == "SwirlyThing" || other.tag == "EventHorizon")   
		{
			return;
		}

		if (explosion != null)
		{
			Instantiate(explosion, transform.position, transform.rotation);
		}

		if (other.tag == "Player")
		{
            GameController.RescueListener();   // Store 3D AudioListener so sounds are not interrupted

            Instantiate(playerExplosion, other.transform.position, other.transform.rotation);

            GameController.GameOver();
		}
		
		GameController.AddScore(scoreValue); 

		Destroy (other.gameObject);

        // Everything gets destroyed in collisons except black holes
        //
        if (gameObject.tag != "SwirlyThing" && !(gameObject.tag.Contains("EventHorizon")))
        {
            Destroy(gameObject); 
        }
	}
}