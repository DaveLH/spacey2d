﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Handle Behaviour of Supernova
/// </summary>
/// 
public class Supernova : MonoBehaviour
{
    public GameObject supernovaPrefab;

    public float ObjectDestroyDelay;  // How far into the explosion animation objects should disappear

    bool m_bSupernovaStarted = false;

	// Use this for initialization
	//void Start () { }


	void Update ()
    {
        /* For debugging only

        const float waitTime = 10.0f;

        if (Time.time > waitTime && !m_bSupernovaStarted)
        */

        if(GameController.HasTimeRunOut && !m_bSupernovaStarted) 
        {
            m_bSupernovaStarted = true;  // Flag ensures supernova is only invoked once

            TriggerExplosion();
        }
	}


    /// <summary>
    /// Spawn Supernova
    /// </summary>
    /// 
    void TriggerExplosion()
    {
        GameObject supernovaExp = GameObject.Instantiate(supernovaPrefab, transform.position, transform.rotation) as GameObject;

        ParticleSystem partsys = supernovaExp.GetComponent<ParticleSystem>();

        partsys.playbackSpeed = 0.5f;

        partsys.Play();

        StartSoundEffect();

        StartCoroutine(VaporizeObjects());   // Use coroutine so animation doesn't stall while objects are disappeared
    }

    /// <summary>
    /// Make objects in scene get destroyed in supernova by tranversing all scene objects and turning 
   ///    all appropriate ones off 
    ///  (Actually destroying them all is probably overkill -- Scene ends right after this anyway)
    /// </summary>
    /// 
    IEnumerator VaporizeObjects()
    {
        GameController.RescueListener();

        yield return new WaitForSeconds(ObjectDestroyDelay);  // Wait until supernova animation is in progress

        foreach (GameObject gob in Object.FindObjectsOfType(typeof(GameObject)))   
        {
            if (gob)
            {
                Debug.Log(gob.name);

                if (gob.tag != "NoSupernova"    &&  
                    gob.tag != "GameController" &&
                    gob.tag != "MainCamera"     && !gob.tag.Contains("Audio"))     // Only objects with these tags are not "vaporized"
                {
                    gob.gameObject.SetActive(false);
                } 
            }

            yield return null;
        }

        GameController.GameOver();
    }


    void StartSoundEffect()
    {
        AudioSource audio = GetComponent<AudioSource>();

        audio.Play();
    }
}
