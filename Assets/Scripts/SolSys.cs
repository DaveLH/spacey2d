﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// This class take the data on generated planets and instantiates prefabs in the game
/// </summary>
/// <remarks>
/// OWNER: PlanetManager (child of GameController)
/// 
/// VALID PLANET TYPES (from class PlanetData):
///   0. Exotic Dwarf (e.g. Io)
///   1. RockyDwarf (e.g. Mercury and typical moons)
///   2. IceDwarf (e.g. Pluto)
///   3. Martian (Desert-y, with possible subsurface water)
///   4. Sub-Earth (Less surface water than Earth)
///   5. Earthlike
///   6. Ocean World (More surface water than Earth)
///   7. Cloud World (like Venus, Titan, or Terrestrial/Jovian hybrid)
///   8. Small gas world (e.g. Uranus and Neptune)
///   9. Saturn-like (smaller, calmer than Jupiter)
///   10. Large gas world (Jupiter-like)
///   11. Brown Dwarf (Transitional bewteen planet and star) 
/// </remarks>
/// 
public class SolSys : MonoBehaviour
{
    public float moveRate;     // How far in units each planet moves in a second
    public float rotateRate;   // Degrees planet rotates in a second

    public GameObject[] planetPrefabs;   // The prefabs to use (see PlanetData.PlanetType enum for ordering)

    public GameObject[] ringPrefabs;   // The prefabs for planetary rings

    public List<PlanetData>.Enumerator PlanetDataEnum;  // Iterate through planet data

    PlanetMover m_planetMover;   // Empty GameObject that drives each planet's motion

    int prevPlanetType;    // Store previous to prevent duplicates


    /// <summary>
    /// Fetch data for current planet
    /// </summary>
    /// 
    public PlanetData CurrentPlanetData
    {
        get { return PlanetDataEnum.Current; }
    }


    /// <summary>
    /// Get total number of generated planets
    /// </summary>
    /// 
    public int PlanetCount
    {
        get { return SolSysData.data.planetsData.Count; }
    }


    // Planet GameObject Factory
    //
    public GameObject InstantiatePlanet(PlanetData pd, Vector3 v3Pos, Quaternion qOrient)
    {
        GameObject prefab = planetPrefabs[pd.PrefabType];

        GameObject gobPlanet = GameObject.Instantiate(prefab, v3Pos, qOrient, this.transform) as GameObject;

        return gobPlanet;
    }


    // Create all planets at once
    //
    public void InstantiateAll()
    {
        foreach(PlanetData pd in SolSysData.data.planetsData)
        {
            InstantiatePlanet(pd, Vector3.right * pd.SMAxis, Quaternion.identity);
        }
    }


    /// <summary>
    /// Get each planet in sequence
    /// </summary>
    /// <param name="gob">Reference to created GameObject, if successful</param>
    /// <returns>
    /// -- TRUE and gob is valid if new planet successfully created
    /// -- TRUE and gob is null  if creation failed or current planet is identical to last
    /// -- FALSE and gob is null if end of list reached. (Winning condition!)
    /// </returns>
    /// 
    public bool InstantiateNextPlanet(ref GameObject gob)
    {
        PlanetData pd;

        // If gob is already a planet, expunge it
        //
        if(gob) Destroy(gob);

        ResetPlanetMover();

        if (PlanetDataEnum.MoveNext())  // Look for Next planet
        {
            pd = PlanetDataEnum.Current;  // Get next planet's data

            if (pd.PrefabType != prevPlanetType)   // Do this test to prevent two identical planets in a row
            {
                // Create planet in scene
                //
                gob = InstantiatePlanet(pd, transform.position, transform.rotation);

                // Attach planet to planet mover
                //
                gob.transform.SetParent(m_planetMover.transform);

                // Create rings, if any
                //
                InstantiateRings(gob, pd.RingType);

                prevPlanetType = pd.PrefabType;  // Store this planet's type for next iteration

                return (gob != null);   // TRUE if planet creation was successful
            }
            else { gob = null;  return true; }   // Don't render planet if it's the same as the last (No planet in current scene)
        }
        else   // End of list -- Time to end game!
        {
            gob = null;

            return false;
        }
    }


    /// <summary>
    /// Instantiate rings
    /// </summary>
    /// 
    void InstantiateRings(GameObject planet, int ringTypeIndex)
    {
        const float ringScaleFactor = 50f;

        if(ringTypeIndex >= 0 && ringTypeIndex < ringPrefabs.Length)
        {
            m_planetMover.transform.Rotate(new Vector3(25.0f, 0.0f, 0.0f));  // Tilt the planet a bit so the rings are visible

            GameObject gob = GameObject.Instantiate(ringPrefabs[ringTypeIndex], planet.transform) as GameObject;

            gob.transform.localPosition = Vector3.zero;
            gob.transform.localScale    = Vector3.one * ringScaleFactor;
            gob.transform.localRotation = Quaternion.Euler(0.0f, 0.0f, 180.0f);
        }
    }


    /// <summary>
    /// Return PlanetMover to starting position (at PlanetManager's transforms)
    /// </summary>
    /// 
    void ResetPlanetMover()
    {
        m_planetMover.transform.position = transform.position;
        m_planetMover.transform.rotation = transform.rotation;
    }


    public void GeneratePlanetData(int MaxPlanetNum)
    {
        // Initialize random planet data
        //
        SolSysData.data.Clear();
        SolSysData.data.QuickGenerate(MaxPlanetNum);
        SolSysData.data.DumpData();

        prevPlanetType = -1;

        // Init. PlanetMover ref
        //
        m_planetMover = GetComponentInChildren<PlanetMover>();

        // Set up planet iterator
        //
        PlanetDataEnum = SolSysData.data.planetsData.GetEnumerator();
    }

}
