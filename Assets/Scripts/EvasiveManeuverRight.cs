﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Green ships always move right
/// </summary>
/// 
public class EvasiveManeuverRight : EvasiveManeuver
{
    protected override float GetTargetManeuver()
    {
        return Random.Range(1, dodge);
    }
}

