﻿using UnityEngine;
using System.Collections;

/// <summary>
/// The Wormhole that player enters to proceed to the next level
/// </summary>
/// 
public class Wormhole : SwirlyThing
{
    // If object is wormhole, proceed to next level, else player "dies"
    //
    public override bool IsWormhole
    {
        get { return true; }
    }

    /// <summary>
    /// Entry of wormhole into scene signals end of level
    /// </summary>
    /// <param name="other"></param>
    /// 
    protected override void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Boundary")
        {
            // Stop spawning enemies
            //
            GameController.StopEnemySpawn();
        }
    }


    protected override void Start()
    {
        fXSpeed = 0.0f;   // Wormhole moves straight ahead only
	}
    

    /*
    protected override void Update()
    {
    }
    
    /// <summary>
    /// Handle what happens when player reaches center of hole --
    ///   Doesn't die, but proceeds to next level.
    /// </summary>
    /// 
    protected override void DeathByBlackHole()
    {
        // TODO: Wait a few seconds
        // TODO: Load next scene/level.
    }
    */
}
