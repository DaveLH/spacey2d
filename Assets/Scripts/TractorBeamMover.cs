﻿using UnityEngine;
using System.Collections;

public class TractorBeamMover : DefaultMover
{
    void OnTriggerEnter (Collider other)
    {
        if (other.tag == "Player") Destroy(gameObject);   // Beam is absorbed by ship
    }
}
