﻿using UnityEngine;
using System.Collections;

public class PlanetData : ScriptableObject
{
    enum PlanetType {
                      ExoticDwarf, RockyDwarf, IceDwarf, Martian, SubEarth, Earthlike,
                      OceanWorld, CloudWorld, Subjovian, GiovePiano, Jovian, BrownDwarf }

    const float GasMassLimit = 5.0f;    // Minimum mass to make a gas giant
    const float IceDistLimit = 3.5f;    // Distance at which small planets are more icey than rocky
    const float GoldieLimit  = 0.6f;    // Minimum Distance at which you can have water-covered planets (IceDistLimit is max)

    public float SMAxis;    // Orbit's semi-major axis (= mean distance from sun)
    public float Mass;      // Mass (Earth = 1)
    public float CoreMass;  // Mass of rocky core only (Earth = 1)
    public float Density;   // Density (H2O = 1)

    public bool m_bGaseous;    // Is planet gaseous (i.e. composed mainly of Hydrogen & Helium)?
    public bool m_bIceWorld;   // Is planet more ice than rock? 

    PlanetType m_plType;    // Type of this planet (Maps to planet Prefabs)

    float m_fRadius;    // Radius of planet (determines size of planet in scene)

    int m_ringType;  // Ring system (Negative = No rings, 0 = Thin rings, 1 = wide rings)

    // bool m_bHasRings    = false;  
    // bool m_bHasSatRings = false;  // Saturn-like (wide) rings?


    public bool IsGasGiant
    {
        get { return (m_bGaseous && Mass > 10f); }
    }

    public bool IsIceDwarf
    {
        get
        {
            return (m_bIceWorld);
        }
    }

    public bool IsRocky
    {
        get
        {
            return (!m_bIceWorld && !m_bGaseous);
        }
    }

    public int PrefabType
    {
        get { return (int)m_plType; }
    }

    public int RingType
    {
        get
        {
            return m_ringType;
        }
    }

    // *** TODO: Moons??


    /// <summary>
    /// Determine planet prefab based on generated data
    /// </summary>
    /// <remarks>TODO: Take other physical properties into account: Density, &c.</remarks>
    /// 
    void SetPlanetType()
    {
        // Gas Giants
        //
        if (IsGasGiant)
        {
            if (Mass > 1000f) m_plType = PlanetType.BrownDwarf;   // "Brown Dwarf"
            //
            else if (Mass > 200f) m_plType = PlanetType.Jovian;   // Jupiter-Like
            //
            else if (Mass > 40f) m_plType = PlanetType.GiovePiano;  // "Quiet Jupiter" (e.g. Saturn)
            //
            else if (Mass > 10f) m_plType = PlanetType.Subjovian;   // Blue-green sub-giant (e.g. Neptune)
            //
            else m_plType = PlanetType.CloudWorld;    // Terrestrial-Jovian hybrid
        }
        else if (IsRocky)   // Rocky, Earth-like planet
        {
            float r = UnityEngine.Random.value;

            if (r < 0.25f) m_plType = PlanetType.ExoticDwarf;   // 25% Rocky worlds will be "exotic" (e.g. Io)
            //    
            else if (r < 0.5f) m_plType = PlanetType.CloudWorld;   // Another 25% will be cloud-coverd, like Venus and Titan
            //
            else if (SMAxis > GoldieLimit && SMAxis < IceDistLimit)   // Water-worlds in "Goldilocks Zone"
            {
                if (Mass > 2.0f) m_plType = PlanetType.OceanWorld;
                //
                else if (Mass > 0.8f) m_plType = PlanetType.Earthlike;
                //
                else if (Mass > 0.1f) m_plType = PlanetType.SubEarth;
                //
                else m_plType = PlanetType.Martian;  // All water is sub-surface
            }
            else m_plType = PlanetType.RockyDwarf;   // The rest are barren and moon-like
        }
        else m_plType = PlanetType.IceDwarf;   // Small and icy, like Pluto
    }


    /// <summary>
    /// Set all secondary data randomly, using mass and orbital distance as start
    /// </summary>
    /// <remarks>TODO: Use more sophisticated algortithm to generate planets, e.g. ACCRETE</remarks>
    /// <param name="mass">Planet's mass (Earth = 1)</param>
    /// <param name="smaxis">Semi-major axis (i.e. mean distance from local star)S</param>
    /// 
    public void SetData()
    {
        // Set Radius
        //
        if(IsGasGiant) m_fRadius = Mathf.Pow(Mass * 5.0f, 0.333f);
        //
        else m_fRadius = Mathf.Pow(CoreMass, 0.333f);

        // TODO: Other physical attriubutes

        // Pick Prefab to render world
        //
        SetPlanetType();

        // Create Rings randomly
        //
        SetRings();

        // TODO: Moons?
    }


    /// <summary>
    /// Does this planet have rings?
    /// </summary>
    /// 
    public void SetRings()
    {
        if(m_plType >= PlanetType.Subjovian)  // Gas giant
        {
            m_ringType = Random.Range(-1, 2);
        }
        else   // Much rarer and more tenuous for smaller worlds
        {
            m_ringType = Random.Range(-3, 1);
        }
    }


    public override string ToString()
    {
        return ("Mass = " + Mass + "; SMAxis = " + SMAxis + "; Type = " + m_plType + "; Rings = " + RingType); 
    }
}
