﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Store fundamental data for planetesimal -- Only enough to run accretion simulation
/// </summary>
/// 
public class ProtoPlanet : ScriptableObject
{
    // Constants
    //
    public const float INNER_EDGE_DFLT = 0.1f;
    public const float OUTER_EDGE_DFLT = 60.0f;
    public const float ECCENTRICITY_COEFF_MIN = 0.07f;   // Dole's default was 0.077 
    public const float ECCENTRICITY_COEFF_MAX = 0.20f;   // Dole's default was 0.077 
    public const float PROTOPLANET_MASS = 1.0E-15f;      // Units of solar masses    
    public const float SOLAR_MASS_IN_GRAMS = 1.989E33f;  // Units of grams          
    public const float EARTH_MASS_IN_GRAMS = 5.977E27f;  // Units of grams    

    public const float EARTH_MASSES_PER_SOLAR_MASS = 332775.64f;

    float mass;   // Mass
    float axis;   // Semi-major axis of orbit
    float eccn;   // Orbital eccentricity

    public const float K = 50.0f;    // K = gas/dust ratio      
    public const float B = 1.2E-5f;  // Used in Crit_mass calc  

    // Initialize planetesimal randomly when it is instantiated
    //
    void Awake ()
    {
        mass = PROTOPLANET_MASS;
        axis = Random.Range(INNER_EDGE_DFLT, OUTER_EDGE_DFLT);
        eccn = Random.Range(ECCENTRICITY_COEFF_MIN, ECCENTRICITY_COEFF_MAX);   
	}

	/// <summary>
    /// Merge another (smaller) protoplanet with this one
    /// </summary>
    /// <param name="other"></param>
    /// 
	public void Merge(ProtoPlanet other)
    {
        float a3, temp;  // "Scratch paper" variables

        // Derive new Semi-Major Axis
        //
        a3 = (Mass + other.Mass) / ((Mass / SemiMajorAxis) + (other.Mass / other.SemiMajorAxis));

        // Derive new Eccentricity
        //
        temp = mass * Mathf.Sqrt(SemiMajorAxis) * Mathf.Sqrt(1.0f - Mathf.Pow(Eccentricity, 2.0f));
        temp = temp + (other.Mass * Mathf.Sqrt(other.SemiMajorAxis) * Mathf.Sqrt(Mathf.Sqrt(1.0f - Mathf.Pow(other.Eccentricity, 2.0f))));
        temp = temp / ((Mass + other.Mass) * Mathf.Sqrt(a3));
        temp = 1.0f - Mathf.Pow(temp, 2.0f);
        //
        if (((temp < 0.0f) || (temp >= 1.0f))) temp = 0.0f;
        //
        Eccentricity = Mathf.Sqrt(temp);

        // Set new Axis
        //
        SemiMajorAxis = a3;

        // And of course, new combined mass
        //
        Mass += other.Mass;
    }


    // Basic attributes
    //
    public float SemiMajorAxis
    {
        get { return axis; }
        set { axis = value; }
    }
    //
    public float Eccentricity
    {
        get { return eccn; }
        set
        {
            // Guarantee that eccentricity is in valid range
            //  (0.0 <= eccen < 1.0)
            //
            eccn = Mathf.Abs(value - Mathf.Floor(value));
        }
    }
    //
    // Mass (Sun = 1.0)
    //
    public float Mass
    {
        get { return mass; }
        set
        {
            mass = Mathf.Abs(value);  // Ensure mass is positive
        }
    }


    // Express as String
    //
    public override string ToString()
    {
        string s = "Mass = " + this.Mass * EARTH_MASSES_PER_SOLAR_MASS + " Earths; Semi-Major Axis = "
                             + this.SemiMajorAxis + "; e = "
                             + this.Eccentricity;

        return s;
    }


    // Find the radii at in which the Planetesimal has gravitational influence
    //   CloudEccentricity = Mean orbital eccentricity of particles in the Solar cloud/nebula
    //
    public float InnerEffectLimit(float fCloudEccentricity)
    {
        return (axis * (1.0f - eccn) * (1.0f - mass) / (1.0f + fCloudEccentricity));
    }
    //
    public float OuterEffectLimit(float fCloudEccentricity)
    {
        return (axis * (1.0f + eccn) * (1.0f + mass) / (1.0f - fCloudEccentricity));
    }
    //
    public float InnerEffectLimit(float fCloudEccentricity, float fReducedMass)
    {
        return (axis * (1.0f - eccn) * (1.0f - fReducedMass) / (1.0f + fCloudEccentricity));
    }
    //
    public float OuterEffectLimit(float fCloudEccentricity, float fReducedMass)
    {
        return (axis * (1.0f + eccn) * (1.0f + fReducedMass) / (1.0f - fCloudEccentricity));
    }


    // Distance of closest approach of object to its primary
    //
    public float Periapsis
    {
        get { return (axis - axis * eccn); }
    }

    // Distance of greatest distance of object from its primary
    //
    public float Apoapsis
    {
        get { return (axis + axis * eccn); }
    }

    // Critical Limit -- The mass at which this planetesimal
    //  can attract gas as well as dust, in which case this planetesimal
    //  will grow into a Jovian ("gas giant") planet
    //  (PORT NOTE: This was a global function in Webb's C++ version)
    //
    public float CriticalLimit
    {
        get
        {
            float temp;

            temp = this.Periapsis * Mathf.Sqrt(SolSysData.data.StarLuminosity);

            return (B * Mathf.Pow(temp, -0.75f));
        }
    }


    /// <summary>
    /// Is this planetesimal to become a gas giant rather than rocky world?
    /// </summary>
    /// 
    public bool IsGasGiant
    {
        get { return (Mass > CriticalLimit); }
    }



    // Planetesimal collects dust from Instance 
    //  Dust is applied to mass via recursive calls
    //
    public float CollectDust(int currentBandIndex)
    {
        float last_mass = Mass;   // Save current mass

        // Scan Instance for local Dust bands and transfer dust
        //  from them to mass of Planetesimal
        //
        // TODO: last_mass = AccreteEngine.Instance.AccumulateDustMass(last_mass, SemiMajorAxis, Eccentricity,
           //                                               CriticalLimit, 0);

        return last_mass;  // TODO: Correct?
    }
}
