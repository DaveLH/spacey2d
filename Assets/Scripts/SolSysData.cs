﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

/// <summary>
/// Class that generates and holds planet database
/// </summary>
/// 
public class SolSysData : ScriptableObject, IAccrete
{
    static SolSysData s_singleton;

    // Attributes of central star 
    // (For brevity, a sun-like star is currently assumed)
    //
    public float StarMass = 1.0f;
    public float StarLuminosity = 1.0f;

    // Editable ACCRETE params (with defaults)
    //
    public float CloudEccentricity = 0.09f;
    public float InnerLimit = 0.3f;
    public float OuterLimit = 60f;
    public float DustArgument = 2.0E-3f;
    public float DustExponent = 3.0f;

    public const float ALPHA = 5.0f;    // Used in density calcs  

    public List<PlanetData> planetsData = new List<PlanetData>();


    /// <summary>
    /// Get unique instance
    /// </summary>
    /// 
    public static SolSysData data
    {
        get
        {
            // Initialize singleton
            //
            if(!s_singleton) s_singleton = ScriptableObject.CreateInstance<SolSysData>();

            return s_singleton;
        }
    }


    /// <summary>
    /// Distance at which ice condenses
    /// </summary>
    /// 
    public float IceCondenRadius
    {
        get { return 4.0f * StarLuminosity * StarLuminosity; }
    }


    /*
    void Awake ()
    {
        s_singleton = this;
    }
    */

    // Get density of Dust (i.e. non-gaseous matter)
    //  at a given radius from the central star
    //
    public float DustDensityAt(float radius)
    {
        // The equation for determining how much dust is available in vicinity of Planetesimal
        //  Note that DustDensityCoeff and DustExp can be tweaked to change the nature of the
        //   system.
        //
        return DustArgument * Mathf.Sqrt(StarMass)
                            * Mathf.Exp(-(ALPHA)
                            * Mathf.Pow(radius, (1.0f / DustExponent)));
    }

    #region "Earth's Twin"-derived methods

    // The following methods generate a random system using "Earth's Twin"
    //   (Creative Computing, Vol 11, No. 11 - Nov. 1985)

    /// <summary>
    /// Generate a "quick and dirty" system of planets
    /// </summary>
    /// <remarks>TODO: Use more sophisticated algortithm to generate planets, e.g. ACCRETE</remarks>
    /// 
    public void QuickGenerate(int MaxPlanetNum)
    {
        int numPlanets = Mathf.Min(MaxPlanetNum, UnityEngine.Random.Range(6, MaxPlanetNum + 1));

        // Extrapolate planets from protoplanetary disk 
        //  (A VERY abbreviated version of what ACCRETE does)
        //
        float fDiskScale = 0.3f * Mathf.Sqrt(StarMass) * UnityEngine.Random.Range(0.2f, 0.9f) * 8.0f / (float)numPlanets;

        // Use a randomized variation on "Bode's Law" to determine a series of potential planet distances
        //  and plug tentative planets at those points...
        //
        foreach(float fBode in BodesLaw(numPlanets, fDiskScale))
        {
            PlanetData pd = ScriptableObject.CreateInstance<PlanetData>();

            pd.SMAxis = fBode;    // Semi-Major Axis for new planet

            Debug.Log("Axis: " + fBode);

            // Set params / attributes for planet based on "mini"-accretion simulation
            //
            AccreteSim(pd);

            // Place in planet list
            //
            planetsData.Add(pd);
        }

        // Scan system, and weed out any planets too close to each other that won't survive
        //
        PlayPoolWithPlanets();

        // Set other attributes for each planet
        //
        SetPlanetAttrbs();
    }


    void SetPlanetAttrbs()
    {
        foreach(PlanetData pd in planetsData)
        {
            pd.SetData();
        }
    }


    /// <summary>
    /// Generate a series of numbers following a randomized variation on "Bode's Law"
    /// </summary>
    /// <remarks>
    /// "Bode's Law" in original form predicts the mean distances of the planets in AU
    /// (Earth = 1) from the sun as such: Distance of Nth planet, D ~= 0.4 for N = 1, 
    /// and D ~= (4 + 3 * 2^(N-2)) / 10, when N > 1.
    /// It works well for almost all the planets in our solar system, 
    /// excepting Neptune and including mean
    /// distances for the Asteroid Belt (N = 5) and the Kuiper Belt (N = 9)
    /// It's really Johann Titius' "Law", but Johann Bode, the Neil DeGrasse Tyson of 
    ///   late 1700's Europe, publicized it. 
    ///  (Footnote: It was also Bode who first proposed the newly discovered seventh planet
    ///    should be named "Uranus"... Clearly he had no familiarity with 21st century
    ///    English-speaking comedians.)
    /// </remarks>
    /// <returns></returns>
    /// 
    public static IEnumerable BodesLaw(int maxPlanetNum, float fFirstOrbitRad)
    {
        float r0 = 0.3f, radius = fFirstOrbitRad;

        yield return fFirstOrbitRad * UnityEngine.Random.Range(0.8f, 1.2f);

        for (int planetNum = 2; planetNum <= maxPlanetNum; planetNum++)
        {
            radius = fFirstOrbitRad + r0 * UnityEngine.Random.Range(0.8f, 1.2f);

            yield return radius;

            r0 = r0 * 2.0f;
        }
    }


    /// <summary>
    /// Clear any existing planets in preparation to generate a new one
    /// </summary>
    /// 
    public void Clear()
    {
        planetsData.Clear();
    }


    public void DumpData()
    {
        foreach (PlanetData pd in planetsData)
        {
            Debug.Log("Planet: " + pd.ToString());
        }
    }


    public static void Test()
    {
        for(float r = 0.1f; r < 60.0f; r += 0.1f)
        {
            Debug.Log("R = " + r + "; D = " + data.DustDensityAt(r));
        }
    }

    #endregion

    #region IAccrete methods

    public void AccreteSim(PlanetData pd)
    {
        float tempMass, coreMass;

        // Accumulate Rocky material
        //
        tempMass = coreMass = CoreDustMass(pd.SMAxis);

        // Accumulate Ice, if far enough from star
        //
        if(pd.SMAxis > IceCondenRadius)
        {
            tempMass = coreMass * 12f;

            pd.m_bIceWorld = true;
        }

        // Accumulate Hyrdogen and Helium, if massive enough
        //
        if (tempMass > 3.0f)
        {
            tempMass = Mathf.Pow(tempMass, 1.5f);

            pd.m_bGaseous = true;
        }

        pd.Mass     = tempMass;
        pd.CoreMass = coreMass;
    }


    public void PlayPoolWithPlanets()
    {
        PlanetData pd = null, prev = null;

        bool finished = false;

        planetsData.Sort(ComparePlanetOrbits);   // Ensure list is sorted by distance from sun, both before and after

        while (!finished)
        {
            for (int i = 1; i < planetsData.Count; i++)
            { 
                prev = planetsData[i - 1];
                pd   = planetsData[i];

                if (prev != null)   // Skip for first planet
                {
                    if (pd.SMAxis - prev.SMAxis < 2.0f)   // If planets are close...
                    {
                        // And if one is big enough...
                        //
                        if (pd.Mass > 100f || prev.Mass > 100f)
                        {
                            // Merge smaller with larger
                            //
                            pd.Mass += prev.Mass;
                            //
                            if (pd.Mass < prev.Mass) pd.SMAxis = prev.SMAxis;  // "prev" with become "current"
                                                                               //
                            planetsData.Remove(prev);

                            i = 0;   // Start loop over
                        }
                    }
                } 
            }

            finished = true;  // Inner loop completed successfully
        }

        planetsData.Sort(ComparePlanetOrbits);   // Ensure list is sorted by distance from sun, both before and after
    }


    private float CoreDustMass(float axis)
    {
        float a2 = axis * axis;

        float coreMass = StarMass * UnityEngine.Random.Range(0.3f, 1.4f) * a2;

        coreMass = coreMass * Mathf.Pow(0.3f, axis / 4.0f);

        return coreMass;
    }


    private static int ComparePlanetOrbits(PlanetData p1, PlanetData p2)
    {
        if (p1.SMAxis < p2.SMAxis) return -1;
        //
        else if (p1.SMAxis > p2.SMAxis) return 1;
        //
        else return 0;
    }


    #endregion
}
