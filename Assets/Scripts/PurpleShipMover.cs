﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Purple ship behaviour -- 
/// 1. Enter scene
/// 2. Pause in forward movement.  Shoot missiles.
/// 3. Proceed forward normally.
/// </summary>
/// 
public class PurpleShipMover : MonoBehaviour
{
    public float speed     = -5;
    public float pauseTime = 10;   // Length of time to pause

    float m_fTimer;   // Pause timer

    bool m_bPaused = false;

    Rigidbody m_rbody;   // Ref to rigid body

    void Awake()
    {
        m_rbody = GetComponent<Rigidbody>();
    }

    void Start()
    {
        m_fTimer = 0.0f;

        pauseTime = GameObject.Find("Game Controller").GetComponent<GameController>().missileShipPause;

        SetNormalSpeed();
    }

    private void SetNormalSpeed()
    {
        m_rbody.velocity = transform.forward * speed;
    }


    void Update()
    {
        // Handle pausing flight for N seconds
        //
        if (m_bPaused)   
        {
            m_fTimer -= Time.deltaTime;

            if (m_fTimer <= 0.0f || GameController.IsGameOver)
            {
                SetNormalSpeed();

                m_bPaused = false;
            }
        }
    }

   
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "TriggerRegion")
        {
            PauseMotion();
        }
    }


    private void PauseMotion()
    {
        m_fTimer = pauseTime;

        m_bPaused = true;

        m_rbody.velocity = Vector3.zero;
    }
}
