﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Specialized Comet movement
/// </summary>
/// 
public class CometMover : DefaultMover
{
    protected bool m_bPaused = false;
    protected bool m_bNormal = true;

    // Set movement state for comet via Pause flag -- Normal flag set internally by Update()
    //   Possible states:
    //     Normal Motion: Normal = true;   Paused = false
    //     Transition to paused: Both true
    //     Paused: Normal = false;   Paused = true
    //     Transition back to Normal: Both false
    //
    public void SetState_Stopped()
    {
        m_bPaused = true;
    }
    //
    public void SetState_Normal()
    {
        m_bPaused = false;
    }


    void Update ()
    {
        if (m_bNormal != m_bPaused) return;  // Non-transitional state -- Do nothing
        //
        else if(m_bPaused)   // Comet is stopped by player beam
        {
            m_bNormal = false;   // Enter Paused state

            SetStopped();
        }
        else if (!m_bNormal)   // Resume motion after player releases comet
        {
            m_bNormal = true;   // Return to normal state

            SetNormalSpeed();
        }
    }


    /// <summary>
    /// Stop coment while player collects resources
    /// </summary>
    /// 
    void SetStopped()
    {
        GetComponent<Rigidbody>().velocity = Vector3.zero;
    }
}
