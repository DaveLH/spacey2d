﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Add left-right movement to asteroids
/// </summary>
/// 
public class AsterMover : DefaultMover
{
    protected float fXSpeed;

    protected override void Start()
    {
        fXSpeed = Random.Range(-1.0f * speed, 1.0f * speed);  // Magnitude of sideways motion

        SetNormalSpeed();
    }

    protected override void SetNormalSpeed()
    {
        GetComponent<Rigidbody>().velocity = new Vector3(fXSpeed, 0.0f, speed);   // Asteroids move at random diagonals
    }
}
