﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Done_Boundary 
{
	public float xMin, xMax, zMin, zMax;
}

public class PlayerController : MonoBehaviour
{
	public float speed;
	public float tilt;
	public Done_Boundary boundary;

	public GameObject tractorSphere;
	public Transform  shotSpawn;
    public GameObject shot;

    public float fireRate;
    
    float m_fNextFire;

    protected Rigidbody m_rigidBody;   // Player's rigid body

    public SwirlyThingAlert holeHandler;   // Handle Black or wormhole that has captured player 

    GameObject m_tractor;   // TractorBeamBubble

    static bool s_bGatherResources = false;

    public static bool GatheringResources
    {
        get { return s_bGatherResources;  }
        set { s_bGatherResources = value; }
    }


    /// <summary>
    /// At start of game play, parent audio listener to player, for "3D" sound effects 
    /// </summary>
    /// <remarks>
    /// CALLED BY: Start()
    /// </remarks>
    /// 
    public void FetchListener()
    {
        Transform trListener = GameController.Listener.transform;

        trListener.SetParent(this.transform);   // Parent to player for "3D" sound
    }

    void Start ()
    {
        speed = GameController.PlayerShipSpeed;

        m_rigidBody = GetComponent<Rigidbody>();

        holeHandler = GetComponent<SwirlyThingAlert>();

        holeHandler.rigidBody = m_rigidBody;

        FetchListener();
    }

    void Update ()
	{
        if (GameController.fireState == GameController.FiringState.Gathering)   // Extracting from comet
        {
            if (m_tractor == null)   // Create "bubble" object
            {
                m_tractor = Instantiate(tractorSphere, transform.position, transform.rotation, transform) as GameObject;
            }
        }
        else if (GameController.fireState == GameController.FiringState.Firing && Time.time > m_fNextFire)   // Firing shots
        {
            m_fNextFire = Time.time + fireRate;

            Instantiate(shot, shotSpawn.position, shotSpawn.rotation);

            GetComponent<AudioSource>().Play();

            GameController.ReduceFuelShot();
        }
	}

	void FixedUpdate ()
	{
        if(!holeHandler.Activated)  
        {
            // Normal movement based on player input
            //  (If holeHandler.Activated is true, then holeHandler.FixedUpdate() executes instead)

            float moveHorizontal = Input.GetAxis("Horizontal");
            float moveVertical   = Input.GetAxis("Vertical");

            Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

            m_rigidBody.velocity = movement * speed;

            m_rigidBody.position = new Vector3(Mathf.Clamp(m_rigidBody.position.x, boundary.xMin, boundary.xMax),
                                               0.0f,
                                               Mathf.Clamp(m_rigidBody.position.z, boundary.zMin, boundary.zMax));

            m_rigidBody.rotation = Quaternion.Euler(0.0f, 0.0f, m_rigidBody.velocity.x * -tilt);

            GameController.ReduceFuelMove(movement.magnitude);
        }
	}
}
