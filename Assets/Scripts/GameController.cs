﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class GameController : MonoBehaviour
{
    public enum FiringState { NotFiring, Firing, Gathering };    // Current state of firing

    static GameController s_singleton;   // Unique instance

	public GameObject[] hazards;
	public Vector3 spawnValues;
	public int hazardCount;
	public float spawnWait;
	public float startWait;
	public float waveWait;
    public float playerSpeed;
    public float missileShipPause = 4.0f;

    public float fuelPerShot     = 0.01f;
    public float fuelPerUnitMvmt = 0.01f;
    public float fuelPerCometCyl = 0.05f;

    public int SceneNum   = 3;
    public int MaxPlanets = 8;

    public Text scoreText;
    public Text distText;
    public Text timeText;
    public Text gameOverText;
    public Text FinalStatusText;
    public Text FinalScoreText;

    public Slider  fuelGadge;
    
    public GameObject audioListener;   // The audio listen for player (must persist when player is blown up)
    public GameObject pauseScreen;     // Menu to bring up when game is paused (<ESC>)
    public GameObject gameOverScreen;  // Menu to bring up at game end

    // Initial position to place planet
    //
    public Vector3 planetPosition;

    public int GameDuration = 10;    // Duration of game in minutes

    SoundManager m_sndMgr;     // Reference to sound manager

    FadeManager m_fadeMgr;     // Reference to fade manager

    SolSys m_solSys;   // Access to generated planet data

    GameObject m_currentPlanet;   // Current planet in scene

    PlanetData m_currentPlanetData;   // Current planet's data

    GameObject m_gameDisplay;   // Hierarchy of GUI elements during normal game play

    private bool m_bGameOver, m_bSpawnEnemies;  // , m_bCometLink;

	private bool m_bOnStartScreen, m_bOnPauseScreen;

	private int m_score;

    private static bool s_bFiringModeComet = false;   // FALSE = Normal shooting; TRUE = Extract from comet

    private float m_fTimeLeft;    // Time left in game
    private float m_fFuel;        // % of avail fuel for ship

    private bool m_bWon;   // Has Player Won game?

    const string SceneNameRoot = "Scene_";
    const string StartScrName  = "Start_Screen";
    const string GameOverMsg   = "Game Over";
    const string GameWonMsg    = "You Won!";
    const string SupernovaMsg  = "You Have Been Vaporized";
    const string MadeItOutMsg  = "You Made It Out of Solar System!";

    /// <summary>
    /// Centralized place to check fire button status
    /// </summary>
    /// 
    public static FiringState fireState
    {
        get
        {
            if (Input.GetButton("Fire1"))
            {
                return (s_bFiringModeComet) ? FiringState.Gathering : FiringState.Firing;
            }
            else return FiringState.NotFiring;
        }
    }

    public static void SetCometState(bool bState)
    {
        s_bFiringModeComet = bState;
    }

    public static bool HasTimeRunOut
    {
        get { return (s_singleton.m_fTimeLeft <= 0.0f); }
    }

    public static bool IsGameOver
    {
        get { return s_singleton.m_bGameOver; }
    }

    public static float PlayerShipSpeed
    {
        get { return s_singleton.playerSpeed; }
    }


    /// <summary>
    /// Get access to audio listener
    /// </summary>
    /// 
    public static AudioListener Listener
    {
        get { return s_singleton.audioListener.GetComponent<AudioListener>(); }
    }


    public static void GetFuelInfo(out float perShot, out float movement, out float cometRefuel)
    {
        perShot     = s_singleton.fuelPerShot;
        movement    = s_singleton.fuelPerUnitMvmt;
        cometRefuel = s_singleton.fuelPerCometCyl;
    }


    /// <summary>
    /// Set up audio stuff
    /// </summary>
    /// 
    void InitAudio()
    {
        // Check that audio listener is present
        //
        if (audioListener == null)
        {
            GameObject gobTemp = GameObject.FindGameObjectWithTag("AudioListener");
            //
            if (gobTemp)
            {
                AudioListener al = gobTemp.GetComponent<AudioListener>();

                // Listener found
                //
                if (al)
                {
                    audioListener = gobTemp;
                }
            } 
        }
    }


    /// <summary>
    /// Initialize game timer to reasonable value for number of worlds in system
    /// </summary>
    /// 
    public void SetTimer()
    {
        if(GameDuration > 0) m_fTimeLeft = GameDuration * 60;  // Give player exactly N minutes (Convert minutes to seconds)
        //
        else m_fTimeLeft = m_solSys.PlanetCount * 90f;  // Give player 90 seconds per scene

        Time.timeScale = 1.0f;   // Normal time flow
    }


    /// <summary>
    /// Update game timer
    /// </summary>
    /// 
    void UpdateTimer()
    {
        const string BaseText = "Time Until Supernova: ";

        m_fTimeLeft -= Time.deltaTime;

        int m = (int)(m_fTimeLeft) / 60;
        int s = (int)(m_fTimeLeft) % 60;

        timeText.text = BaseText + string.Format("{0:00}:{1:00}", m, s);
    }


    void Awake ()
    {
        m_bOnPauseScreen = false;
        m_bOnStartScreen = false;

        m_fadeMgr = GetComponent<FadeManager>();
        m_sndMgr  = GetComponent<SoundManager>();
        m_solSys  = GetComponentInChildren<SolSys>();

        m_gameDisplay = GameObject.Find("Game Display");
        m_gameDisplay.SetActive(false);

        s_singleton = this;

        DontDestroyOnLoad(this);   // The data here carries over into the game scenes
    }


    void Start ()
	{
        if(!m_bOnStartScreen)
        {
            // Start playing music right away
            //
            InitAudio();

            // Display Start Screen
            //
            GoStartMenu();
        }
	}
	

	void Update ()
	{
		if (m_bGameOver)   // If game over and menu is displayed
        {
            HandleGameOverKeys();   // Handle keystrokes
        }
        else if (!m_bOnStartScreen)   // Only during game play
        {
            UpdateTimer();

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                PauseMenu();   // Handle pause menu
            } 
        }
	}




    /// <summary>
    /// Start a new level by launching random scene and loading next 
    ///   generated planet from PlanetManager
    /// </summary>
    /// <remarks>
    /// CALLED BY: StartNextLevel()
    /// </remarks>
    /// 
    public void LaunchLevel()
    {
        m_bOnStartScreen = false;

        if (m_bOnPauseScreen) // If restarted from Pause menu... 
        {
            rescueListener();  // Stash Listener for player reset
            PauseMenu();       // Dismiss Pause menu
        }

        int sceneToLoad = Random.Range(1, SceneNum + 1);

        string SceneName = SceneNameRoot + sceneToLoad.ToString();

        SceneManager.LoadScene(SceneName);

        m_gameDisplay.SetActive(true);

        bool success = m_solSys.InstantiateNextPlanet(ref m_currentPlanet);

        if (success)
        {
            m_currentPlanetData = m_solSys.CurrentPlanetData;
        }
        else    // No more planets -- Game won!
        {
            m_bWon = true;

            GameOver();
        }

        scoreText.enabled = true;
        // restartText.enabled = true;
        gameOverText.enabled = true;

        m_bSpawnEnemies = true;

        // restartText.text = "";
        // gameOverText.text = "";

        UpdateScore();
        UpdateDistDisplay();

        StartCoroutine(SpawnWaves());
    }


    /// <summary>
    /// Spawn enemies in waves (Coroutine)
    /// </summary>
    /// <remarks>
    /// See video at: https://unity3d.com/learn/tutorials/projects/space-shooter/spawning-waves?playlist=17147
    /// </remarks>
    /// 
    IEnumerator SpawnWaves ()
	{
		yield return new WaitForSeconds (startWait);   // Wait time at start of game

		while (m_bSpawnEnemies)
		{
			for (int i = 0; i < hazardCount; i++)
			{
				GameObject hazard = hazards [Random.Range (0, hazards.Length)];   // Fetch random hazard prefab

                // Spawn at random point
                //
				Vector3 spawnPosition = new Vector3 (Random.Range (-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);

                // Zero rotation
                //
				Quaternion spawnRotation = Quaternion.identity;

                // Create the hazard
                //
				Instantiate (hazard, spawnPosition, spawnRotation);

                // Wait before spawning the next hazard
                //
                yield return new WaitForSeconds (spawnWait);
			}

            // Wait before spawning the next WAVE of hazards
            //
            yield return new WaitForSeconds (waveWait);
		}
	}


    /// <summary>
    /// Toggle Suspend game and show pause menu
    /// </summary>
    /// 
    public void PauseMenu()
    {
        m_bOnPauseScreen = !m_bOnPauseScreen;   // Toggle

        Time.timeScale = (m_bOnPauseScreen) ? 0.0f : 1.0f;   // Freeze/Unfreeze the game

        m_sndMgr.MuteEffects(m_bOnPauseScreen);   // Mute sound effects while game suspended

        pauseScreen.SetActive(m_bOnPauseScreen);
    }


    /// <summary>
    /// Show/Hide Game Over Screen
    /// </summary>
    /// 
    public void GameOverMenu()
    {
        Time.timeScale = (m_bGameOver) ? 0.0f : 1.0f;   // Freeze the game until menu exits

        gameOverScreen.SetActive(m_bGameOver);
    }


    public void SetGameOverText()
    {
        if(m_bWon)   // Tell player they won
        {
               gameOverText.text = GameWonMsg;
            FinalStatusText.text = MadeItOutMsg;   
        }
        else   // Better luck next time!
        {
               gameOverText.text = GameOverMsg;
            FinalStatusText.text = SupernovaMsg;
        }

        FinalScoreText.text = string.Format("Your Score: {0:0000}", m_score);
    }


    /// <summary>
    /// Handle Keystrokes on "Game Over" Screen
    /// </summary>
    /// 
    public void HandleGameOverKeys()
    {
        if(m_bGameOver)   // Only check keys if "Game Over" screen is active
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
                StartPlaying();   // Restart game
            }
            else if (Input.GetKeyDown(KeyCode.S))
            {
                GoStartMenu();   // Return to starting (settngs) menu
            }
            else if (  Input.GetKeyDown(KeyCode.Q) || 
                      (Input.GetKeyDown(KeyCode.F4) && (Input.GetKey(KeyCode.LeftAlt) || Input.GetKey(KeyCode.RightAlt))))
            {
                Quit();   // "Q" or "Alt-F4" quits
            }
        }
    }


    /// <summary>
    /// Go to start menu, either at game start or at user request
    /// </summary>
    /// 
    public void GoStartMenu()
    {
        m_bOnPauseScreen = false;

        m_bGameOver = false;
        //
        GameOverMenu();  // Dismiss GameOver menu, if it's currently displayed

        m_gameDisplay.SetActive(false);

        rescueListener();  // Ensure AudioListener will not be interrupted with scene change

        SceneManager.LoadScene(StartScrName);   // (Re)Load Start Scene

        m_bOnStartScreen = true;
    }


    /// <summary>
    /// IF player loses, or game returns to start menu, prevent audio listener from being destroyed with Player
    /// </summary>
    /// <remarks>
    /// CALLED BY: Player=>DestroyByContact->OnTriggerEnter()
    /// </remarks>
    /// 
    public void rescueListener()
    {
        Transform trListener = audioListener.transform;

        // Move to background object for "safe keeping" (It gets moved back when game restarts)
        //
        trListener.SetParent(this.transform);
    }
    //
    public static void RescueListener()
    {
        s_singleton.rescueListener();
    }


    public static void AddScore (int newScoreValue)
	{
		s_singleton.m_score += newScoreValue;

        s_singleton.UpdateScore();
	}
	
	void UpdateScore ()
	{
        const string BaseText = "Score: ";

        scoreText.text = BaseText + m_score;
	}


    void UpdateDistDisplay()
    {
        const string BaseText = "Distance from sun: ";

        distText.text = BaseText + string.Format("{0:0.0} AU", m_currentPlanetData.SMAxis);
    }


    public static void ReduceFuelMove(float moveMagnitude)
    {
        s_singleton.m_fFuel -= moveMagnitude * s_singleton.fuelPerUnitMvmt;

        s_singleton.UpdateFuelGadge();

        if (s_singleton.m_fFuel <= s_singleton.fuelGadge.minValue)
        {
            // TODO: Handle Out of fuel state
        }
    }


    public static void ReduceFuelShot()
    {
        s_singleton.m_fFuel -= s_singleton.fuelPerShot;

        s_singleton.UpdateFuelGadge();

        if (s_singleton.m_fFuel <= s_singleton.fuelGadge.minValue)
        {
            // TODO: Handle Out of fuel state
        }
    }


    public static bool AddFuel()
    {
        if (s_singleton.m_fFuel < s_singleton.fuelGadge.maxValue)
        {
            s_singleton.m_fFuel += s_singleton.fuelPerCometCyl;

            s_singleton.UpdateFuelGadge();

            return true;
        }

        return false;   // Fuel is at capacity
    }

        
    void UpdateFuelGadge()
    {
        fuelGadge.value = m_fFuel;
    }


    #region Fetch params from Start Screen controls 

    public void GetParam_waveWait(string sRawInput)
    {
        try
        {
            waveWait = float.Parse(sRawInput);
        }
        catch (System.FormatException)
        {

            Debug.Log("TODO: Put up error dialog");
        }
    }

    public void GetParam_hazardCount(string sRawInput)
    {
        try
        {
            hazardCount = int.Parse(sRawInput);
        }
        catch (System.FormatException)
        {

            Debug.Log("TODO: Put up error dialog");
        }
    }

    public void GetParam_PlayerSpeed(float fSpeed)
    {
        playerSpeed = fSpeed;
    }

#endregion

#region Starting and Ending the Game

    /// <summary>
    /// Initialize and start gameplay
    /// </summary>
    /// <remarks>
    /// CALLED BY: Click "PLAY" on start screen, and at game restart 
    /// </remarks>
    /// 
    public void StartPlaying()
    {
        m_score = 0;
        m_fFuel = 1.0f;

        m_bWon      = false;
        m_bGameOver = false;

        m_solSys.GeneratePlanetData(MaxPlanets);   // Generate new system at each restart

        GameOverMenu();   // Dismiss GameOver menu, if it's currently displayed

        SetTimer();
        StartNextLevel();
    }


    /// <summary>
    /// End this level and start the next
    /// </summary>
    /// <remarks>
    /// CALLED BY: Player flying into wormhole, ending the level
    /// </remarks>
    /// 
    public static void StartNextLevel()
    {
        RescueListener();

        s_singleton.LaunchLevel();

        // StartFadeIn();
    }


    /// <summary>
    /// Halt Game Play
    /// </summary>
    /// 
    public static void GameOver()
    {
        s_singleton.m_bGameOver = true;

        float gameEndDelay = (s_singleton.m_bWon) ? 0.0f : 4.0f;

        StopEnemySpawn();
        RescueListener();

        s_singleton.SetGameOverText();

        s_singleton.Invoke("GameOverMenu", gameEndDelay);
    }

    
    public static void StopEnemySpawn()
    {
        s_singleton.m_bSpawnEnemies = false;
    }


    public static void StartFadeIn()
    {
        s_singleton.m_fadeMgr.StartFadeIn();
    }

    public static void StartFadeOut()
    {
        s_singleton.m_fadeMgr.StartFadeOut();
    }


    /// <summary>
    /// Handle quitting of the game (Only works in final game build)
    /// </summary>
    /// 
    public void Quit()
    {
        Debug.Log("Quitting...");

        GameOverMenu();   // Dismiss menu

        Listener.enabled = false;   // Turn off sound

        Application.Quit();
    }

    #endregion
}