﻿using UnityEngine;
using System.Collections;

public class Done_WeaponController : MonoBehaviour
{
	public GameObject shot;
	public Transform shotSpawn;
	public float fireRate;
	public float delay;

	void Start ()
	{
		InvokeRepeating ("Fire", delay, fireRate);
	}

	void Fire ()
	{
        if (!GameController.IsGameOver)   // Stop shooting if player is destroyed
        {
            Instantiate(shot, shotSpawn.position, shotSpawn.rotation);

            GetComponent<AudioSource>().Play(); 
        }
	}
}
