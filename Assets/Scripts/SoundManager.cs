﻿using UnityEngine;
using UnityEngine.Audio;
using System.Collections;

/// <summary>
/// Handle commands related to sound
/// </summary>
/// 
public class SoundManager : MonoBehaviour
{
    AudioMixer m_mixer;

    float m_fPrevFxVol, m_fPrevMusicVol;   // Store previous volumes on mute, so thaey can be restored

    const float MuteLevel = -80.0f;    // Level at which sound is effectively zero (muted)
    const float MaxLevel  =  20.0f;    // Maximum Level

    const float MaxVol = 10f;   // Maximum volume set by user

    /* Audio adjustment (called by controls on start screen) */

    public void GetParam_MasterVolume(float fVol)
    {
        float Range = Mathf.Abs(MaxLevel - MuteLevel);   // Full span of volume

        float fVolume = MuteLevel + ((fVol / MaxVol) * Range);   // Convert user input to valid range

        m_mixer.SetFloat("VolMaster", fVolume);   // Set global volume
    }

    public void GetParam_MuteMusic(bool bMuted)
    {
        if (bMuted)   // Mute turned on
        {
            m_mixer.GetFloat("VolMusic", out m_fPrevMusicVol);  // Save old volume

            m_mixer.SetFloat("VolMusic", MuteLevel);  // Mute sound
        }
        else  // Mute turned off -- Restore former volume
        {
            m_mixer.SetFloat("VolMusic", m_fPrevMusicVol);
        }
    }


    public void GetParam_MuteEffects(bool bMuted)
    {
        MuteEffects(bMuted);
    }


    public void MuteEffects(bool bMuted)
    {
        if (bMuted)   // Mute turned on
        {
            m_mixer.GetFloat("VolFX", out m_fPrevFxVol);  // Save old volume

            m_mixer.SetFloat("VolFX", MuteLevel);  // Mute sound
        }
        else  // Mute turned off -- Restore former volume
        {
            m_mixer.SetFloat("VolFX", m_fPrevFxVol);
        }
    }


    void Start ()
    {
        // Fetch ref to Mixer asset from Background Music (child object)
        //
        m_mixer = GetComponentInChildren<AudioSource>().outputAudioMixerGroup.audioMixer;
    }

	/*
	// Update is called once per frame
    //
	void Update ()
    {
	    // TODO: Implement ability for user to adjust volume or mute
	}
    */
}
