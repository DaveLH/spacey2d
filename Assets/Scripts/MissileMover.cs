﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Control the missile's "homing" movement
/// </summary>
/// <remarks>
/// "We would like to assure you that the guided missiles currently converging 
///   with your ship are merely a courtesy detail..."
/// </remarks>
/// 
public class MissileMover : MonoBehaviour
{
    public float velocity    =  5;
    public float turn        =  2;
    public float maxDistance = 10;   // Player must be closer than this to be chased

    Rigidbody m_rbody;

    Transform m_trTarget;    // Ref to Player's transform


    void Start ()
    {
        m_rbody = GetComponent<Rigidbody>();

        GameObject gobPlayer = GameObject.FindGameObjectWithTag("Player");

        if (gobPlayer != null)   // Player has not been destroyed
        {
             m_trTarget = gobPlayer.transform;
        }
        else m_trTarget = null;
	}


    void Update()
    {
        if(GameController.IsGameOver)
        {
            Destroy(this.gameObject);   // Stop any existing missiles
        }
    }


    void FixedUpdate ()
    {
        m_rbody.velocity = transform.forward * velocity;

        if (m_trTarget != null)   // Player has not been blown up
        {
            float fDist = Vector3.Distance(transform.position, m_trTarget.position);  // Distance to player

            if (fDist > maxDistance) return;
            //
            else   // Head for target
            {
                // m_rbody.velocity = transform.forward * velocity;

                Quaternion qRot = Quaternion.LookRotation(m_trTarget.position - transform.position);

                m_rbody.MoveRotation(Quaternion.RotateTowards(transform.rotation, qRot, turn));
            } 
        }
	}
}
