﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Move planet through the scene (ship appears to fly past it),
///   and rortate it.
/// </summary>
/// 
public class PlanetMover : MonoBehaviour
{
    public float moveRate;     // How far in units planet moves in a second
    public float rotateRate;   // Degrees planet rotates in a second

    private float m_z;

    void Start ()
    {
        m_z = transform.position.z;
    }
	
	void Update ()
    {
        m_z = (moveRate * Time.deltaTime);

        // Move planet "forward"
        //
        transform.Translate(0f, 0f, m_z, Space.World);

        // Rotate planet
        //
        transform.Rotate(Vector3.up, rotateRate * Time.deltaTime);
	}
}
