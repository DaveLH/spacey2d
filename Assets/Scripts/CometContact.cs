﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Handle when a comet comes in contact with other objects
/// </summary>
/// 
public class CometContact : MonoBehaviour
{
    public GameObject explosion;
    public GameObject playerExplosion;
    public GameObject tractorBeam;

    public int scoreValue = 1;

    public Transform trBoltSpawn;

    bool m_bFuelingUp   = false;   // Is player extracting from Comet?

    public float boltDelay = 0.25f;   // Time delay between bolts

    GameController gameController;

    CometMover cometMover;    // Comet motion control


    void Start()
    {
        cometMover = GetComponent<CometMover>();

        trBoltSpawn = transform.FindChild("BoltsSpawn");

        // gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
    }


    void Update()
    {
        if (m_bFuelingUp)
        {
            if (GameController.fireState == GameController.FiringState.NotFiring)
            {
                FuelUpStop();  // Releasing fire button ends fueling
            }
        }
    }


    void FuelUpStart()
    {
        m_bFuelingUp = true;

        GameController.SetCometState(true);

        cometMover.SetState_Stopped();   // Comet stops during fueling

        InvokeRepeating("DrawFuelFromComet", 0.1f, boltDelay);   // Run routing to do fueling up
    }


    void DrawFuelFromComet()
    {
        Instantiate(tractorBeam, trBoltSpawn.position, trBoltSpawn.rotation);

        GetComponent<AudioSource>().Play();

        bool bAddMoreFuel = GameController.AddFuel();   // Returns false when full capacity

        if (!bAddMoreFuel) FuelUpStop();
    }


    void FuelUpStop()
    {
        CancelInvoke();   // End fueling up sequence

        cometMover.SetState_Normal();  // Coment starts moving again

        GameController.SetCometState(false);

        m_bFuelingUp = false;
    }

    /*
    IEnumerator SpawnBolt()
    {
        while (m_bFuelingUp && Input.GetButton("Fire1"))  // Resources are extracted while fire button is held down
        {
            yield return new WaitForSeconds(boltDelay);  // Slight gap between bolts

            Instantiate(tractorBeam, trBoltSpawn.position, trBoltSpawn.rotation);

            GetComponent<AudioSource>().Play(); 
        }
    }
    */


    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            GameController.RescueListener();   // Store 3D AudioListener so sounds are not interrupted

            Instantiate(playerExplosion, other.transform.position, other.transform.rotation);

            Destroy(other.gameObject);

            GameController.GameOver();
        }
        else if (other.tag == "Enemy")   // Enemies can destroy comet to prevent player from refuelling
        {
            Instantiate(explosion, transform.position, transform.rotation);

            Destroy(gameObject);
        }
        else if(other.tag == "PlayerBolt" && !m_bFuelingUp)
        {
            FuelUpStart();

            // GameController.AddScore(scoreValue);
        }

        // Else Comet just continues on its merry way (even if it hits and destroys player)...
    }
}
