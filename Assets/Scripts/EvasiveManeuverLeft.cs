﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Red ships always move left
/// </summary>
/// 
public class EvasiveManeuverLeft : EvasiveManeuver
{
    protected override float GetTargetManeuver()
    {
        return Random.Range(1, dodge) * -1;
    }
}

