﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Ostensible special effect -- Mainly to create a collider to "catch" tractor beam
/// </summary>
/// 
public class TractorBeamBubble : MonoBehaviour
{
	void Update ()
	{
		// Stop when button is released or fuel is full
        //
        if(GameController.fireState != GameController.FiringState.Gathering)
        {
            Destroy(gameObject);
        }
	}

    void OnTriggerEnter (Collider other)
    {
        Destroy(other.gameObject);   // Beam is absorbed by ship
    }
}
