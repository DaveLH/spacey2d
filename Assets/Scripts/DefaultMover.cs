﻿using UnityEngine;
using System.Collections;

public class DefaultMover : MonoBehaviour
{
	public float speed;

	protected virtual void Start ()
	{
        SetNormalSpeed();
    }


    /// <summary>
    /// Default is for objects to move straight ahead
    /// </summary>
    /// 
    protected virtual void SetNormalSpeed()
    {
        GetComponent<Rigidbody>().velocity = transform.forward * speed;
    }
}
