﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/// <summary>
/// Handle Scene fade in/out
/// </summary>
/// <remarks>
/// OWNER: Fade Panel
/// </remarks>
/// 
public class FadeManager : MonoBehaviour
{
    public GameObject fadeScreen, fadePanel;
        
    Image m_fadeImg;

    public float fadeSpeed;    // Positive = Fade Out to black; Negative = Fade in

    public float delay;   // Delay to start fade out in seconds (ignored on fade in)

	void Start ()
    {
        m_fadeImg = fadePanel.GetComponent<Image>();
	}
	
    IEnumerator doFadeIn()
    {
        Color color;
        float alpha;
        float endColor = 0.0f;

        fadeScreen.SetActive(true);

        while(m_fadeImg.color.a > endColor)
        {
            color = m_fadeImg.color;

            alpha = m_fadeImg.color.a - Time.deltaTime / fadeSpeed;

            m_fadeImg.color = new Color(color.r, color.g, color.b, alpha);

            yield return null;
        }

        fadeScreen.SetActive(false);
    }

    IEnumerator doFadeOut()
    {
        Color color;
        float alpha;
        float endColor = 1.0f;

        fadeScreen.SetActive(true);

        while (m_fadeImg.color.a < endColor)
        {
            color = m_fadeImg.color;

            alpha = m_fadeImg.color.a + Time.deltaTime / fadeSpeed;

            m_fadeImg.color = new Color(color.r, color.g, color.b, alpha);

            yield return null;
        }

        fadeScreen.SetActive(false);
    }

    public void StartFadeIn()
    {
        StartCoroutine(doFadeIn());
    }

    void startFadeOut()
    {
        StartCoroutine(doFadeOut());
    }

    public void StartFadeOut()
    {
        Invoke("startFadeOut", delay);
    }

    // Update is called once per frame 
    void Update () {
	
	}
}
